#!/usr/bin/env node

let crypto = require('crypto');

// launch the command line data
command = process.argv[2]
if (command == 'key') {
    // generate a random AES key
    let keyBytes = crypto.randomBytes(16);
    console.log(keyBytes.toString('base64'));
} else if (command == 'encrypt') {
    // pull the core data
    let keyBytes = Buffer.from(process.argv[3], 'base64');
    let toEncrypt = process.argv[4];

    // generate an IV
    let iv = crypto.randomBytes(16);

    // execute the encryption
    let cipher = crypto.createCipheriv('aes-256-cbc', keyBytes, iv);
    let encrypted = cipher.update(toEncrypt, 'utf8');
    cipher.final();

    let withIv = Buffer.concat([iv, encrypted]);

    // spit out the encrypted value
    console.log(withIv.toString('base64'));
} else if (command == 'decrypt') {
    // pull the core data
    let keyBytes = Buffer.from(process.argv[3], 'base64');
    let encryptedBytes = Buffer.from(process.argv[4], 'base64');

    // segregate the data
    let iv = encryptedBytes.slice(0, 16);
    let encryptedData = encryptedBytes.slice(16);

    // execute the decryption
    let decipher = crypto.createDecipheriv('aes-256-cbc', keyBytes, iv);
    var decrypted = decipher.update(encryptedData);
    decrypted += decipher.final();

    // spit out the decrypted value
    console.log(decrypted);
} else {
    console.error("I don't know how to \"" + command + "\"");
    process.exit(1);
}
