import base64
from Crypto.Cipher import AES


class Decryptor:

    @staticmethod
    def decrypt(key, encrypted):
        # decode the key
        key_bytes = base64.b64decode(key.encode('utf-8'))

        # segregate the IV and raw data
        encrypted_bytes = base64.b64decode(encrypted.encode('utf-8'))
        iv = encrypted_bytes[:16]
        encrypted_data = encrypted_bytes[16:]

        # perform the decryption
        cipher = AES.new(key_bytes, AES.MODE_CBC, iv)
        decrypted_data = cipher.decrypt(encrypted_data)

        # remove the padding & restore
        return Decryptor.pkcs5trim(decrypted_data).decode('utf-8')

    @staticmethod
    def pkcs5trim(data):
        return data[:-data[-1]]
