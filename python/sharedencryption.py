import sys
from key_generator import KeyGenerator
from encryptor import Encryptor
from decryptor import Decryptor


def main():
    command = sys.argv[1]
    if command == 'key':
        print(KeyGenerator.generate_key())
    elif command == 'encrypt':
        print(Encryptor.encrypt(sys.argv[2], sys.argv[3]))
    elif command == 'decrypt':
        print(Decryptor.decrypt(sys.argv[2], sys.argv[3]))
    else:
        print("I don't know how to \"" + command + '".')
        return 1


if __name__ == '__main__':
    sys.exit(main())
