import base64
from Crypto.Cipher import AES
from Crypto import Random


class Encryptor:

    @staticmethod
    def encrypt(key, to_encrypt):
        # decode the data to encrypt
        raw_data = Encryptor.pkcs5pad(to_encrypt)

        # decode the key
        key_bytes = base64.b64decode(key.encode('utf-8'))

        # actually run the encryption
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(key_bytes, AES.MODE_CBC, iv)

        # add the IV to the front of the encrypted data & encode in Base 64
        full_data_set = (iv + cipher.encrypt(raw_data))
        base64_encoded = base64.urlsafe_b64encode(full_data_set)
        return base64_encoded.decode('utf-8')

    @staticmethod
    def pkcs5pad(raw_data):
        return raw_data + (16 - len(raw_data) % 16) * chr(16 - len(raw_data) % 16)
