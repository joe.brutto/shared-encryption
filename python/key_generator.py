import base64
from Crypto import Random


class KeyGenerator:

    @staticmethod
    def generate_key():
        byte_array = Random.get_random_bytes(16)
        base64_encoded = base64.urlsafe_b64encode(byte_array)
        return base64_encoded.decode('utf-8')
