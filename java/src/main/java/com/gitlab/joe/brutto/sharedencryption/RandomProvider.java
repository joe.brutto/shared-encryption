package com.gitlab.joe.brutto.sharedencryption;

import java.security.GeneralSecurityException;
import java.security.SecureRandom;

/**
 * Interface that defines how we handle requests for {@link SecureRandom} instances.
 * We explicitly support the raising of security exceptions as this library (or
 * "example set," really) is focused on keeping things secure. We have a default
 * provider for you but realize that this may not be feasible to use on every
 * platform out there without some assistance.
 */
@FunctionalInterface
public interface RandomProvider {

  /**
   * Requests that a new {@code Random} be generated.
   * @return the newly-generated {@code Random} instance
   * @throws GeneralSecurityException if anything goes wrong generating the instance
   */
  SecureRandom getRandom() throws GeneralSecurityException;

  /**
   * Our default provider that uses a SHA-1 PRNG algorithm to generate it.
   */
  final class Default implements RandomProvider {

    /**
     * {@inheritDoc}
     */
    @Override
    public SecureRandom getRandom() throws GeneralSecurityException {
      return SecureRandom.getInstance("SHA1PRNG");
    }
  }
}
