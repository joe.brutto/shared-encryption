package com.gitlab.joe.brutto.sharedencryption;

/**
 * Main class providing us with the ability to make requests to our application in
 * order to provide data for testing. Java is the starting point in our project so
 * we have multiple things that users can ask for us to generate. For additional
 * information on what to do, run the jar and pass in the {@code --help} flag for
 * additional detail.
 */
public class Main {

  /**
   * Primary entry point for the application.
   * @param args command line arguments passed into the application
   * @throws Exception if anything goes horribly wrong
   */
  public static void main(String[] args) throws Exception {
    // assume everything is correct - CLI parsing in libraries sucks compared to things like Go with mow.cli
    switch (args[0]) {
      case "key":
        System.out.println(Encryptor.generateBase64Key());
        break;
      case "encrypt":
        System.out.println(Encryptor.encryptAndBase64Encode(args[2], args[1]));
        break;
      case "decrypt":
        System.out.println(Encryptor.decrypt(args[2], args[1]));
        break;
      default:
        System.err.println(String.format("I don't know how to \"%s\"", args[0]));
        System.exit(1);
    }
  }
}
