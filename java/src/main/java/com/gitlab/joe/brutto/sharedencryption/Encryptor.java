package com.gitlab.joe.brutto.sharedencryption;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

// TODO: add support for injection of RandomProvider

/**
 * Class that knows how to encrypt and decrypt incoming data.
 */
public class Encryptor {

  /**
   * The key length to use as the default for encryption.
   */
  private static final int KEY_LENGTH = 256;

  /**
   * Algorithm being used for keys.
   */
  private static final String KEY_ALGORITHM = "AES";

  /**
   * Algorithm being used in the cipher.
   */
  private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5PADDING";

  /**
   * Generates a new key of the hard-coded length.
   * @return the bytes of the generated key
   * @throws GeneralSecurityException if there is any error generating the key
   */
  public static byte[] generateKey() throws GeneralSecurityException {
    KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_ALGORITHM);
    keyGenerator.init(KEY_LENGTH);
    return keyGenerator.generateKey().getEncoded();
  }

  /**
   * Generates a new key of the hard-coded length and Base 64 encodes it.
   * @return the Base 64 encoded version of the key
   * @throws GeneralSecurityException if there is any error generating the key
   */
  public static String generateBase64Key() throws GeneralSecurityException {
    byte[] keyBytes = generateKey();
    return Base64.getEncoder().encodeToString(keyBytes);
  }

  /**
   * Generates a randomized IV based on the algorithm for the Cipher.
   * @return a randomized IV based on the algorithm for the Cipher
   * @throws GeneralSecurityException if anything goes wrong during generation
   */
  public static byte[] generateIv() throws GeneralSecurityException {
    SecureRandom random = new RandomProvider.Default().getRandom();

    Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
    int blockSize = cipher.getBlockSize();

    byte[] iv = new byte[blockSize];
    random.nextBytes(iv);
    return iv;
  }

  /**
   * Encrypts the given value and returns it as a Base 64-encoded string.
   * @param value the value that we want to encode
   * @param key the key string to use for decryption
   * @return the encoded data string
   * @throws GeneralSecurityException if anything goes wrong during generation
   */
  public static String encryptAndBase64Encode(String value, String key) throws GeneralSecurityException {
    // decrypt the key
    byte[] keyBytes = Base64.getDecoder().decode(key);

    // seed & encrypt
    byte[] iv = generateIv();
    byte[] encrypted = encrypt(value, iv, keyBytes);

    // create a string where the first 16 bytes are the IV and the rest is the encrypted string
    assert iv.length == 16 : String.format("Invalid IV length (%d)", iv.length);

    int totalSize = iv.length + encrypted.length;
    ByteBuffer buffer = ByteBuffer.allocate(totalSize).put(iv).put(encrypted).rewind();

    // encode the string and return it
    return Base64.getEncoder().encodeToString(buffer.array());
  }

  /**
   * Decodes the given incoming Base 64-encoded string using the provided key.
   * @param incoming the incoming string to decrypt (Base 64-encoded)
   * @param key the key string to use for decryption
   * @return the decrypted string from the provided data
   * @throws GeneralSecurityException if anything goes wrong during generation
   */
  public static String decrypt(String incoming, String key) throws GeneralSecurityException {
    // decode the key
    byte[] keyBytes = Base64.getDecoder().decode(key);

    // the incoming string has the IV in the first 16 bytes and the rest is what we want to decrypt
    // ... but it comes in Base64
    byte[] incomingBytes = Base64.getDecoder().decode(incoming);
    byte[] ivBytes = Arrays.copyOfRange(incomingBytes, 0, 16);
    byte[] toDecryptBytes = Arrays.copyOfRange(incomingBytes, 16, incomingBytes.length);

    // decrypt
    IvParameterSpec iv = new IvParameterSpec(ivBytes);
    SecretKeySpec secretKey = new SecretKeySpec(keyBytes, KEY_ALGORITHM);

    Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
    cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
    byte[] decryptedBytes = cipher.doFinal(toDecryptBytes);

    // return the string decrypted
    return new String(decryptedBytes, StandardCharsets.UTF_8);
  }


  /**
   * Encrypts the given string within the confines of the given data.
   * @param value the raw value to encrypt
   * @param iv the seed to use for the encryption
   * @param key the bytes for the key to use for encryption/decryption
   * @return the encoded value
   * @throws GeneralSecurityException if anything goes horribly wrong
   */
  private static byte[] encrypt(String value, byte[] iv, byte[] key) throws GeneralSecurityException {
    // encrypt
    IvParameterSpec ivSpec = new IvParameterSpec(iv);
    SecretKeySpec secretKey = new SecretKeySpec(key, KEY_ALGORITHM);

    Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
    cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
    return cipher.doFinal(value.getBytes(StandardCharsets.UTF_8));
  }
}
