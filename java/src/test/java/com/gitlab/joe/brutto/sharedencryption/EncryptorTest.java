package com.gitlab.joe.brutto.sharedencryption;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.security.GeneralSecurityException;
import java.util.UUID;

/**
 * Tests our actual encryption routines that we wrote this thing for.
 */
@Test(
  description = "Tests our actual encryption routines that we wrote this thing for."
)
public class EncryptorTest {

  /**
   * Tests the generation of keys for encryption.
   * @throws GeneralSecurityException if anything goes horribly wrong
   */
  @Test (
    description = "Tests the generation of keys for encryption.",
    invocationCount = 25
  )
  public void testKeyGeneration() throws GeneralSecurityException {
    String key = Encryptor.generateBase64Key();
    Assert.assertTrue(StringUtils.isNotBlank(key), "Generated a blank key (!?)");
  }

  /**
   * Tests the generation of IV (salt) data for encryption.
   * @throws GeneralSecurityException if anything goes horribly wrong
   */
  @Test (
    description = "Tests the generation of IV (salt) data for encryption.",
    invocationCount = 25
  )
  public void testGenerateIv() throws GeneralSecurityException {
    byte[] iv = Encryptor.generateIv();
    Assert.assertTrue(iv.length > 0, "Generated a zero-length IV (!?)");
  }

  /**
   * Tests encryption/decryption of data
   * @throws Exception if anything goes horribly wrong
   */
  @Test (
    description = "Tests encryption/decryption of data",
    invocationCount = 25
  )
  public void testEncryptDecrypt() throws Exception {
    String input = UUID.randomUUID().toString();
    String key = Encryptor.generateBase64Key();

    String encryptedString = Encryptor.encryptAndBase64Encode(input, key);
    String decryptedString = Encryptor.decrypt(encryptedString, key);

    Assert.assertEquals(decryptedString, input, "Decryption returned invalid string");
  }
}
