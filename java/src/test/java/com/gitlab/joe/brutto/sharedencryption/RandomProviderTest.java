package com.gitlab.joe.brutto.sharedencryption;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.security.GeneralSecurityException;
import java.security.SecureRandom;

/**
 * Tests our generate for {@code Random} instances.
 */
@Test (
  description = "Tests our generate for {@code Random} instances."
)
public class RandomProviderTest {

  /**
   * Tests basic generation of a {@code Random} from the default factory setup.
   * @throws GeneralSecurityException if anything goes horribly wrong
   */
  public void testDefaultGeneration() throws GeneralSecurityException {
    SecureRandom instance = new RandomProvider.Default().getRandom();
    Assert.assertNotNull(instance, "Failed to generate a Random instance");
  }
}
