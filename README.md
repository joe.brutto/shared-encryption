# Shared Encryption

Set of examples showing the use of a shared encryption key between languages.

 * **Key**: AES
 * **Cipher**: AES/CBC/PKCS5PADDING
 * **Key Length**: 256

We start with Java as the base and pass the data encrypted there to our other languages.

## The Process

 1. Build all projects
 2. Execute the Java project build artifact to generate a key
 3. Encrypt and store a random value using the Java project artifact
 4. Verify decryption with the Java project artifact
 5. Pass the encryption key and encrypted value to each of the other languages verifying it decrypts properly

## Some Notes

 * We store the IV in the front of the encrypted data
 * All data is Base 64 encoded
