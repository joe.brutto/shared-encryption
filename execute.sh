#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# cleans-up any message we leave around
cleanUp() {
  cd "${SCRIPT_DIR}"
}

# exit on error with message
exitOnError() {
  local EXIT_CODE="$1"
  local ERROR_MESSAGE="$2"

  if [[ "${EXIT_CODE}" != "0" ]]; then
    echo ""
    echo ""
    echo -e "\e[1m\e[91mFAILURE\e[0m: ${ERROR_MESSAGE}"
    echo ""
    cleanUp
    exit ${EXIT_CODE}
  fi
}

# project build setup
buildProject() {
  local PROJECT_DIR="$1"
  local BUILD_COMMAND="$2"

  echo "  -> ${PROJECT_DIR}"

  local BUILD_DIRECTORY="${SCRIPT_DIR}/${PROJECT_DIR}"
  local BUILD_LOG_DIRECTORY="${BUILD_DIRECTORY}/logs"

  cd "${BUILD_DIRECTORY}"

  if [[ ! -e "${BUILD_LOG_DIRECTORY}" ]]; then
    mkdir -p "${BUILD_LOG_DIRECTORY}"
  fi

  ${BUILD_COMMAND} >"${BUILD_LOG_DIRECTORY}/stdout.log" 2>"${BUILD_LOG_DIRECTORY}/stderr.log"
  exitOnError $? "Unable to build ${PROJECT_DIR} project (see logs in \"${BUILD_LOG_DIRECTORY}\")"

  rm -Rf "${BUILD_LOG_DIRECTORY}"
  cd "${SCRIPT_DIR}"
}

# function to validate decryption of data
validateDecryption() {
  local PROJECT_DIR="$1"
  local EXPECTED_RESULT="$2"
  local EXECUTION_COMMAND="$3"

  echo "  -> ${PROJECT_DIR}"

  local BUILD_DIRECTORY="${SCRIPT_DIR}/${PROJECT_DIR}"
  local BUILD_LOG_DIRECTORY="${BUILD_DIRECTORY}/logs"

  cd "${BUILD_DIRECTORY}"

  if [[ ! -e "${BUILD_LOG_DIRECTORY}" ]]; then
    mkdir -p "${BUILD_LOG_DIRECTORY}"
  fi

  ${EXECUTION_COMMAND} >"${BUILD_LOG_DIRECTORY}/stdout.log" 2>"${BUILD_LOG_DIRECTORY}/stderr.log"
  exitOnError $? "Unable to decrypt in ${PROJECT_DIR} project (see logs in \"${BUILD_LOG_DIRECTORY}\")"

  local DECRYPTION_RESULT=$(cat "${BUILD_LOG_DIRECTORY}/stdout.log")
  
  if [ "${DECRYPTION_RESULT}" != "${EXPECTED_RESULT}" ]; then
    exitOnError 3 "Expected \"${EXPECTED_RESULT}\" but got \"${DECRYPTION_RESULT}\""
  fi

  rm -Rf "${BUILD_LOG_DIRECTORY}"
  cd "${SCRIPT_DIR}"
}

# generate a random string to be encrypted
STRING_TO_ENCRYPT=$( cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 1024 | head -n 1)

# say hello
echo -e "\e[33mApplication Dir:   \e[0m${SCRIPT_DIR}"
echo -e "\e[33mRunning As:        \e[0m$(whoami)"
echo -e "\e[33mLaunch Timestamp:  \e[0m$(date)"
echo ""
echo -e "\e[33mData to Encrypt:   \e[0m${STRING_TO_ENCRYPT}"
echo ""

# build the projects one-by-one
echo ""
echo -e "\e[96mBuilding projects\e[0m"
echo ""

buildProject "java" "mvn -U clean package"
buildProject "go" "/bin/bash buildAndTest.sh"

# load the Java encryption data
#  ... here we assume the key generation and basic encryption will never fail   :-/
echo ""
echo -e "\e[96mLoading/Creating base encryption data\e[0m"
echo ""

ENCRYPTION_KEY=$(java -jar java/target/SharedEncryption.jar key)
ENCRYPTED_STRING=$(java -jar java/target/SharedEncryption.jar encrypt "${ENCRYPTION_KEY}" "${STRING_TO_ENCRYPT}")

echo -e "  -> encryption key:   \e[94m${ENCRYPTION_KEY}\e[0m"
echo -e "  -> encrypted string: \e[94m${ENCRYPTED_STRING}\e[0m"

# test the decryption from the various sources
echo ""
echo -e "\e[96mTesting decryption from source\e[0m"
echo ""

validateDecryption "java" "${STRING_TO_ENCRYPT}" "java -jar target/SharedEncryption.jar decrypt ${ENCRYPTION_KEY} ${ENCRYPTED_STRING}"
validateDecryption "go" "${STRING_TO_ENCRYPT}" "./build/sharedencryption decrypt ${ENCRYPTION_KEY} ${ENCRYPTED_STRING}"
validateDecryption "python" "${STRING_TO_ENCRYPT}" "python3 sharedencryption.py decrypt ${ENCRYPTION_KEY} ${ENCRYPTED_STRING}"
validateDecryption "node" "${STRING_TO_ENCRYPT}" "node . decrypt ${ENCRYPTION_KEY} ${ENCRYPTED_STRING}"

# if we got here, all is good
echo ""
echo ""
echo -e "\e[92mSUCCESS\e[0m"
echo ""
cleanUp
