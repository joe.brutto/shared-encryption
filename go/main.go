package main

import (
  "fmt"
  cli "github.com/jawher/mow.cli"
  "os"
  "shared_encryption/encryption"
)

func main() {
  // setup the command line arguments
  app := cli.App("sharedencryption", "Shared encryption examples")
  app.Command("key", "Generates a Base 64 encoded key to use for encryption", generateKey)
  app.Command("encrypt", "Encrypts an input string read from standard in (argument is the Base 64 encryption key to use)", encrypt)
  app.Command("decrypt", "Decrypts an input string read from standard in (argument is the Base 64 encryption key to use)", decrypt)

  // actually run
  err := app.Run(os.Args)
  if err != nil {
    panic(err)
  }
}

// Generates a new key to use for encryption.
func generateKey(_ *cli.Cmd) {
  key, err := encryption.GenerateKey()
  if err != nil {
    panic(err)
  }
  fmt.Println(*key)
}

// encrypts an incoming data string from stdin.
func encrypt(cmd *cli.Cmd) {
  encryptionKey := cmd.StringArg("ENCRYPTION_KEY", "", "The key to use for encryption")
  plainString := cmd.StringArg("ENCRYPTED_STRING", "", "The string to encrypt")

  cmd.Action = func() {
    result, err := encryption.Encrypt(*encryptionKey, *plainString)
    if err != nil {
      panic(err)
    }
    fmt.Println(*result)
  }
}

// decrypts an incoming data string from stdin
func decrypt(cmd *cli.Cmd) {
  encryptionKey := cmd.StringArg("ENCRYPTION_KEY", "", "The key to use for decryption")
  encryptedString := cmd.StringArg("ENCRYPTED_STRING", "", "The string to decrypt")

  cmd.Action = func() {
    result, err := encryption.Decrypt(*encryptionKey, *encryptedString)
    if err != nil {
      panic(err)
    }
    fmt.Println(*result)
  }
}
