#!/usr/bin/env bash

exitOnError() {
  local EXIT_CODE=$1
  local ERROR_MESSAGE="$2"
  if [ "${EXIT_CODE}" != "0" ]; then
    echo "FAILURE: ${ERROR_MESSAGE}"
    exit "${EXIT_CODE}"
  fi
}

# clean the cache
go clean -testcache
exitOnError $? "Error cleaning test cache"

# test ignoring the cache
go test ./...
exitOnError $? "At least one test failed"

# build an executable
CGO_ENABLED=0 go build -installsuffix 'status' -o ./build/sharedencryption
exitOnError $? "Unable to build executable"
