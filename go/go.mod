module shared_encryption

go 1.12

require (
	github.com/jawher/mow.cli v1.1.0
	github.com/stretchr/testify v1.3.0
)
