package encryption

import (
  "bytes"
  "crypto/aes"
  "crypto/cipher"
  "crypto/rand"
  "encoding/base64"
)

// Executes the encryption routine for the AES/CBC/PKCS5PADDING combination. This
// assumes that the key provided is Base 64 encoded and that we want the IV as the
// first 16 bytes of the encrypted data. Once completed we Base 64 the encoded
// string before it is returned. Any errors we encounter are returned unless there's
// some kind of weird panic under the covers that we didn't expect to encounter.
func Encrypt(encryptionKey string, toEncrypt string) (*string, error) {
  // load the key bytes
  keyBytes, err := base64.StdEncoding.DecodeString(encryptionKey)
  if err != nil {
    return nil, err
  }

  // seed
  iv, err := generateIv()
  if err != nil {
    return nil, err
  }

  // encrypt
  block, err := aes.NewCipher(keyBytes)
  if err != nil {
    return nil, err
  }

  ecb := cipher.NewCBCEncrypter(block, iv)

  content := []byte(toEncrypt)
  content = pkcs5Padding(content, block.BlockSize())
  encrypted := make([]byte, len(content))
  ecb.CryptBlocks(encrypted, content)

  // add the IV to the front of the encrypted bytes
  var ivAndData []byte
  ivAndData = append(ivAndData, iv...)
  ivAndData = append(ivAndData, encrypted...)

  // base 64 encode and exit
  encodedString := base64.StdEncoding.EncodeToString(ivAndData)
  return &encodedString, nil
}

// Generates a 16 byte IV.
func generateIv() ([]byte, error) {
  data := make([]byte, 16)
  _, err := rand.Read(data)
  return data, err
}

// Performs PKCS5 padding against the provided data.
func pkcs5Padding(cipherText []byte, blockSize int) []byte {
  padding := blockSize - len(cipherText) % blockSize
  paddingText := bytes.Repeat([]byte{byte(padding)}, padding)
  return append(cipherText, paddingText...)
}
