package encryption

import (
  "github.com/stretchr/testify/assert"
  "testing"
)

// Tests decryption (and also encryption as a byproduct).
func TestDecrypt(t *testing.T) {
  assertT := assert.New(t)

  // generate a key
  key, err := GenerateKey()
  assertT.Nil(err, "Unable to generate a key")

  // encrypt a bunch of strings
  for index := 0; index < 500; index++ {
    toEncrypt := randomString()

    // encrypt the data
    encrypted, err := Encrypt(*key, toEncrypt)
    assertT.Nil(err, "Error encrypting the string")
    assertT.NotNil(encrypted, "Generated a nil string (!?)")

    // decrypt the data
    decrypted, err := Decrypt(*key, *encrypted)
    assertT.Nil(err, "Error decrypting the string")
    assertT.NotNil(decrypted, "Generated a nil string (!?)")
    assertT.Equal(toEncrypt, *decrypted, "Invalid data decrypted")
  }
}

// Tests decryption with data from a known, external source
func TestDecryptWithKnown(t *testing.T) {
  assertT := assert.New(t)

  key := "PzyGfVaXsHmcswRq7W31GRZRetoicomy2JTjgDhXDg0="
  encryptedString := "+bCEn4NZYsXcLYJkl9vnNE9NntmBijlyKSR9B57ydyU="
  expectedResult := "poop"

  decrypted, err := Decrypt(key, encryptedString)
  assertT.Nil(err, "Error decrypting the string")
  assertT.NotNil(decrypted, "Generated a nil string (!?)")
  assertT.Equal(expectedResult, *decrypted, "Invalid data decrypted")
}
