package encryption

import (
  "crypto/rand"
  "encoding/base64"
)

const KeyLength = 32

// Generates a Base 64 encoded AES key of the default length.
func GenerateKey() (*string, error) {
  keyBytes := make([]byte, KeyLength)
  _, err := rand.Read(keyBytes)
  if err != nil {
    return nil, err
  }

  encodedKey := base64.StdEncoding.EncodeToString(keyBytes)
  return &encodedKey, nil
}
