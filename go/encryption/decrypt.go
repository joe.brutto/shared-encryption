package encryption

import (
  "crypto/aes"
  "crypto/cipher"
  "encoding/base64"
)

// Executes the decryption routine for the AES/CBC/PKCS5PADDING combination. This
// assumes that the data provided is Base 64 encoded and that we have the IV as the
// first 16 bytes of the encrypted data once it is decoded from Base 64. Any errors
// we encounter are returned unless there's some kind of weird panic under the
// covers that we didn't expect to encounter.
func Decrypt(encryptionKey string, encryptedString string) (*string, error) {
  // load the key bytes
  keyBytes, err := base64.StdEncoding.DecodeString(encryptionKey)
  if err != nil {
    return nil, err
  }

  // decode from base 64
  incomingBytes, err := base64.StdEncoding.DecodeString(encryptedString)
  if err != nil {
    return nil, err
  }

  // segregate the data
  ivBytes := incomingBytes[:16]
  toDecryptBytes := incomingBytes[16:]

  // initialize the cipher data
  block, err := aes.NewCipher(keyBytes)
  if err != nil {
    return nil, err
  }

  ecb := cipher.NewCBCDecrypter(block, ivBytes)

  // execute decryption
  decrypted := make([]byte, len(toDecryptBytes))
  ecb.CryptBlocks(decrypted, toDecryptBytes)
  decryptedString := string(pkcs5Trimming(decrypted))

  // return the decrypted data
  return &decryptedString, nil
}

// Performs PKCS5 trimming against the provided data.
func pkcs5Trimming(encrypt []byte) []byte {
  padding := encrypt[len(encrypt) - 1]
  return encrypt[:len(encrypt) - int(padding)]
}
