package encryption

import (
  "github.com/stretchr/testify/assert"
  "math/rand"
  "testing"
)

// Tests basic data encryption (without decryption). We just check for basic errors
// here and assume that decryption tests will handle the testing of the to-and-from
// pattern associated with the methodology.
func TestEncrypt(t *testing.T) {
  assertT := assert.New(t)

  // generate a key
  key, err := GenerateKey()
  assertT.Nil(err, "Unable to generate a key")

  // encrypt a bunch of strings
  for index := 0; index < 500; index++ {
    toEncrypt := randomString()
    encrypted, err := Encrypt(*key, toEncrypt)
    assertT.Nil(err, "Error encrypting the string")
    assertT.NotNil(encrypted, "Generated a nil string (!?)")
  }
}

// Utility function for random string generation.
func randomString() string {
  runes := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890`~!@#$%^&*()_+-=[]{};':")
  dataArray := make([]rune, 256)
  for index := range dataArray {
    dataArray[index] = runes[rand.Intn(len(runes))]
  }
  return string(dataArray)
}
