package encryption

import (
  "github.com/stretchr/testify/assert"
  "testing"
)

// Tests the generation of key data
func TestGenerateKey(t *testing.T) {
  assertT := assert.New(t)

  for index := 0; index < 500; index++ {
    key, err := GenerateKey()
    assertT.Nil(err, "Error generating key")
    assertT.NotNil(key, "Generate a nil key (!?)")
  }
}
